<?php

declare(strict_types=1);

namespace Drupal\importmaps;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;

/**
 * Defines a plugin manager for importmaps.
 */
final class ImportMapsManager extends DefaultPluginManager implements PluginManagerInterface {

  /**
   * Constructs an ImportMapsManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   Module extension list.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, protected ?ModuleExtensionList $moduleExtensionList) {
    $this->alterInfo('importmaps');
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'importmaps', ['importmaps']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('importmaps', $this->moduleHandler->getModuleDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    // Make the path relative to the module.
    if (str_starts_with($definition['path'], '/')) {
      return;
    }
    $provider = $this->extractProviderFromDefinition($definition);
    $definition['path'] = sprintf('/%s/%s', $this->moduleExtensionList->getPath($provider), $definition['path']);
  }

}
