<?php

declare(strict_types=1);

namespace Drupal\Tests\importmaps\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests importmap functionality.
 *
 * @group importmaps
 */
final class ImportMapsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['importmaps', 'importmaps_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function testImportMaps(): void {
    $this->drupalGet('/');
    $el = $this->assertSession()->elementExists('css', 'script[type="importmap"]');
    $this->assertEquals([
      'imports' => [
        'relative' => sprintf('/%s/js/relative.js', \Drupal::service('extension.list.module')->getPath('importmaps_test')),
        'absolute' => '/absolute.js',
      ],
    ], Json::decode($el->getText()));
  }

}
